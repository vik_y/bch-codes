% lab 4_3 
% Benchmarks till now: 
% tada.wav 1 bit 42 sec
% tada.wav 2 bit 71 sec 
% GIGGLE.WAV 1 bit 13 sec
% GIGGLE.WAV 2 bit 23 sec
% chime.wav 1 bit 4 sec
% chime.wav 2 bit 7 sec
% Need to optimize 

x=1;
p = 0.01; % with probability p, make an error 

% Ask user for input
x = input('1 or 2 error correction?\n');
p = input('Enter probability: 0 to 0.4\n');

k=11;
if (x==2)
    k=7;
end

[y,fs,nbits]=wavread('chime.wav');

blockSize = nbits

% take the mono waveform
y1=y(:,1); 
start = find(y1>0);
start(1);
start(end);
y1 = y1(start(1):start(end));
sound(y1,fs);
%subplot(2,1, 1)
%plot(y1);

y1 = y1 * 32768; % to get absolute value 

length(y1);
x = [ zeros(1, length(y1)*16)];

% Generating bit stream and storing it in x 
for i= 1:length(y1)
    temp = de2bi(abs(y1(i, :)), 'left-msb');
    if(y1(i)>0)
        i_x = [ zeros(1, 16-length(temp)) temp];
    else
        i_x = [1 zeros(1, 16-length(temp)-1) temp];
    end
    x((i-1)*blockSize+1:i*blockSize)= i_x;
end 

% Pad x 
npad = k - mod(length(x), k); % Padding
x = [x zeros(1, npad)]; % Padding done 

N = length(x);
decoded = [zeros(1, length(x))];

for i=1:N/k
    temp = x((i-1)*k+1:i*k);
    encoded = BCHEncoder( x((i-1)*k+1:i*k) , length(temp));% Encoding the block
    e = 1*(rand(size(encoded)) < p);
    r_x = mod(encoded + e, 2);
    % error was introduced after encoding using BSC logic

    [d i_x]= BCHDecoder(r_x, k);
    decoded((i-1)*k+1:i*k)= i_x;
end

[num_bit,ratio_bit] = biterr(decoded, x)
output = [zeros(length(y1), 1)];

for i= 1:length(y1)
    temp = bi2de(decoded((i-1)*16+2:i*blockSize), 'left-msb');
    if(decoded((i-1)*16+1)==1)
        temp = -temp;
    end
    output(i) = temp;
end


output = output/32768;
check = output - y1;
length(find(check~=0))

sound(output, fs)

%figure(1)
%hold on
%stem(probability, ratio)
%xlabel('P of error: BSC Channel')
%ylabel('BER')
%hold off





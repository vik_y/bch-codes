function [i_x] = pulseDecoder(transmitted, pulse)
	
    result = [zeros(1, floor(length(transmitted)/8))];
    forconv = [zeros(1, floor(length(transmitted)/8)*15)];
    count=1;
    
    %prealloc = [zeros(1, 15)];
    
    for i=[1:1:length(transmitted)/8]
        %prealloc(1:8) = transmitted((i-1)*8+1:i*8);
        %prealloc(9:15) = zeros(1, 7);
        forconv((i-1)*15+1:i*15) = [transmitted((i-1)*8+1:i*8) zeros(1, 7)];
        %forconv((i-1)*15+1:i*15) = prealloc;
    end
    temp = conv(forconv, pulse);
    temp = temp(1:floor(length(transmitted)/8)*15);
    for i=[1:1:length(transmitted)/8]
        c = temp((i-1)*15+8);
        
        if(c>0)
            result(count) = 1;
        else
            result(count) = 0;
        end
        count = count+1;
    end
    
     i_x = result;
end

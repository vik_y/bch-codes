function [i_x] = pulseGenerator(symbol, pulse)
	output = [zeros(1, length(symbol))];
    pad = repmat(output, 7, 1);
    c = [symbol ; pad];
    c = reshape(c, [1, numel(c)]);
    i_x = conv(c, pulse);
    i_x = i_x(1:length(symbol)*8);
end







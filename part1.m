% part1

blockSize = 31;
N = 10000000;
N = N + blockSize - mod(N, blockSize); % Padding

x = randi([0 1],1, N); % Generating random code word of size N
p = 0.3; % with probability p, make an error 

errorbits = 1*(rand(size(x)) < p); % error with probability 0.1 introduced

% reshape 
        
blocks = reshape(errorbits, blockSize, N/blockSize);
errorCount = [0 0 0];

for i=1:N/blockSize
    temp = blocks(:, i);
    %length(find(temp>0));
    
    cal = [0; temp; 0];
    r = diff(find(cal==0))-1;
    e = r(r>0);
    
    if(any(e==1))
        errorCount(1) = errorCount(1)+1;
    end
    
    if(any(e==2))
        errorCount(2) = errorCount(2)+1;
    end
    
    if(any(e==3))
        errorCount(3) = errorCount(3)+1;
    end
    
    errorCount;

end

errors = [errorCount*100/N];
errors

hold on
plot([1, 2, 3], errors)
stem([1, 2, 3], errors, '.')
xlabel('No. of errors in block')
ylabel('Percentage of blocks')




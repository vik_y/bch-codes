k = 11;
blockSize = 15

N = 100000;
N = N + k - mod(N, k); % Padding

snr = [-5 -2 0 2 5 8 12 15]
x = randi([0 1],1, N); % Generating random code word of total size N
ratio = [];
ratio0 = [];
ratio7 = [];
encoded = []

for j=snr
    encoded = 2*x-1;
    encoded = awgn(encoded, j);
    encoded(encoded>=0)=1;
   	encoded(encoded<0)=0;
    [num_bit,ratio_bit] = biterr(encoded, x);
    ratio0 = [ratio0 ratio_bit];
end
for j=snr
    decoded = [zeros(1, N)]; % allocating memory to speed up program
    for i=1:N/k
        temp = x((i-1)*k+1:i*k);
        
        encoded = BCHEncoder( x((i-1)*k+1:i*k) , length(temp));% Encoding the block
        % map symbols first
        encoded = encoded *2 -1 ;
        
        % awgn with snr
        encoded = awgn(encoded, j);
        
        % mapping symbols back 
        
   		encoded(encoded>=0)=1;
   		encoded(encoded<0)=0;
            
        [d, i_x]= BCHDecoder(encoded, k);
        decoded((i-1)*k+1:i*k)= i_x;
    end
    decoded;
    [num_bit,ratio_bit] = biterr(decoded, x);
    ratio = [ratio ratio_bit];
end

k = 7
for j=snr
    decoded = [zeros(1, N)]; % allocating memory to speed up program
    for i=1:N/k
        temp = x((i-1)*k+1:i*k);
        
        encoded = BCHEncoder( x((i-1)*k+1:i*k) , length(temp));% Encoding the block
        % map symbols first
        encoded = encoded *2 -1 ;
        
        % awgn with snr
        encoded = awgn(encoded, j);
        
        % mapping symbols back 
        
   		encoded(encoded>=0)=1;
   		encoded(encoded<0)=0;
   		
   		
        [d i_x]= BCHDecoder(encoded, k);
        decoded((i-1)*k+1:i*k)= i_x;
    end
    decoded;
    [num_bit,ratio_bit] = biterr(decoded, x);
    ratio7 = [ratio7 ratio_bit];
end

snr

figure(1);
ratio 
ratio0
semilogy(snr, ratio0, 'r')
hold on;
semilogy(snr, ratio,'g')
hold on;
semilogy(snr, ratio7, 'b')
hold on;
ylabel('BER');
xlabel('SNR');
title('BER vs SNR');
%semilogy(pr,a1,'r--')
%set(gca(), 'xdir','reverse');
xlim([-5, 15])
legend('No Coding', 'BCH(15, 11)', 'BCH(15, 7)')
grid on;

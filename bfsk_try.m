% bfsk_try.m

M=2;
x = randi([0 M-1],1000,1); % Generating random number with 0 and 1 
Freq_sep=1600; % Hz % 
Nsamp = 8;
Fs =6400; % Hz

y=fskmod(x,M,Freq_sep,Nsamp,Fs);

freq=0:Fs/(length(y)-1):Fs;
figure(1)
plot(y)

figure(2)
plot(freq,abs(fft(y)));
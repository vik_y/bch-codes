k = 11;
blockSize = 15

N = 20;
N = N + k - mod(N, k); % Padding

probability = [0:0.05:0.4]
x = randi([0 1],1, N); % Generating random code word of total size N
ratio = [];


for j=probability
    p = j; % with probability p, make an error 
    % errorbits = 1*(rand(N/k*blockSize) < p); % error with probability 0.1 introduced
    decoded = [zeros(1, N)];
    for i=1:N/k
        temp = x((i-1)*k+1:i*k);
        
        encoded = BCHEncoder( x((i-1)*k+1:i*k) , length(temp));% Encoding the block
        e = 1*(rand(size(encoded)) < p);
        r_x = mod(encoded + e, 2);
        % error was introduced after encoding using BSC logic

        [d i_x]= BCHDecoder(r_x, k);
        decoded((i-1)*k+1:i*k)= i_x;
    end
    decoded;
    [num_bit,ratio_bit] = biterr(decoded, x);
    ratio = [ratio ratio_bit];
end

%figure(1)
%hold on
%stem(probability, ratio)
%xlabel('P of error: BSC Channel')
%ylabel('BER')
%hold off


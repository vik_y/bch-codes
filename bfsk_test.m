% bfsk_test.m
% BER vs. SNR for FSK

 
SNR=-20:3:10; %in dB
snr = 10.^(SNR/10); % actual ratio
N=10^3; % number of bits
M=2; % number of levels in the signal. 
x = randi([0 M-1],N,1);
Freq_sep=1600; % Hz
Nsamp = 8;
Fs =6400; % Hz
 
% Generate fskmod signal
y=fskmod(x,M,Freq_sep,Nsamp,Fs);
signal_power=mean(abs(y).^2)*Nsamp;
noise_power = (signal_power./snr);
 
for k=1:length(SNR)
noise = (noise_power(k)/sqrt(2)).*(randn(1,N*Nsamp)+sqrt(-1)*randn(1,N*Nsamp));
rcvd = y + noise';
 
Z = fskdemod(rcvd,M,Freq_sep,Nsamp,Fs);
errors(k) = sum(abs(x-Z));
end
 
semilogy(SNR,errors/N);
grid
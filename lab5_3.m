% lab5_3.m
% BER vs. SNR for FSK

 
SNR=-20:3:10; %in dB
snr = 10.^(SNR/10); % actual ratio
N=10^4; % number of bits
M=2; % number of levels in the signal. 
x = randi([0 M-1],N,1);
Freq_sep=1600; % Hz
Nsamp = 8;
Fs =6400; % Hz
 
% Generate fskmod signal - No encoding 
y=fskmod(x,M,Freq_sep,Nsamp,Fs);
signal_power=mean(abs(y).^2)*Nsamp;
noise_power = (signal_power./snr);
 
for n=1:length(SNR)
noise = (noise_power(n)/sqrt(2)).*(randn(1,N*Nsamp)+sqrt(-1)*randn(1,N*Nsamp));

rcvd = y + noise';
 
Z = fskdemod(rcvd,M,Freq_sep,Nsamp,Fs);
errors(n) = sum(abs(x-Z));
end
 

c = [11 7];
blockSize = 15;

ratio = [];
ratio7 = [];

for k=c
    for n=1:length(SNR)
        enc = zeros(1, floor(N/k)*15); % allocating memory to speed up program
        for i=1:N/k
            temp = x((i-1)*k+1:i*k);
            encoded = BCHEncoder( x((i-1)*k+1:i*k) , length(temp));
            enc((i-1)*15+1:i*15)= encoded;
        end
        % BCH Encoding Done  

        % Use FSK MOD Here 
        y=fskmod(enc,M,Freq_sep,Nsamp,Fs);
        signal_power=mean(abs(y).^2)*Nsamp;
        noise_power = (signal_power./snr); 


        % Demodulation of FSK 
        noise = (noise_power(n)/sqrt(2)).*(randn(1,length(enc)*Nsamp)+sqrt(-1)*randn(1,length(enc)*Nsamp));
        
        rcvd = y + noise;
        Z = fskdemod(rcvd,M,Freq_sep,Nsamp,Fs);

        %BCH Decoding 
        decoded = [zeros(1, N)];
        for i=1:N/k    
            [d, i_x]= BCHDecoder(Z((i-1)*15+1:i*15), k);
            decoded((i-1)*k+1:i*k)= i_x;
        end

        decoded = reshape(decoded, N, 1);

        if k==11
            ratio = [ratio sum(abs(x-decoded))/N]; %Error calculation
        else 
            ratio7 = [ratio7 sum(abs(x-decoded))/N]; %Error calculation
        end
    end
end

ratio
ratio7
semilogy(SNR,errors/N, 'r');
hold on
semilogy(SNR, ratio,'b')
hold on;
semilogy(snr, ratio7, 'black')
hold on;
ylabel('BER');
xlabel('SNR');
title('BER vs SNR');
xlim([-20, 15])
legend('No Coding', 'BCH(15, 11)', 'BCH(15, 7)')
grid on;



    
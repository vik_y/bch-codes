function [ encodedCode ] = BCHEncoder( i_x, k )
	% i_x = Information that needs to be encoded 
	% k  tells which code to use (15, 11) or (15, 7)
	n = 15;
	g_x = [];	
	if k==11
		g_x = [1 1 0 0 1] ;% Generator function created : x4+x+1 [1 1 0 0 1]
	end

	if k==7
		g_x = [1 0 0 0 1 0 1 1 1];  % Generator function created: 1+x4+x6+x7+x8
	end
	u_x = mod(conv(g_x, i_x), 2); % Basically: u(x) = g(x) * i(x)
	encodedCode = u_x;
end


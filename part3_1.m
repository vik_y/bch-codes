%part3_1
input=randi([0 1],1,10000);

a=[];
a1=[];
pr=[];
x=[0.2,0.1,0.01,0.005,0.001];
for p=x;
   [ber]=bch15_7(input,p);
    a=[a;ber];
   [ber1]=bch15_11(input,p);
    a1=[a1;ber1];
   pr=[pr;p];
   temp = x((i-1)*k+1:i*k);

    encoded = BCHEncoder( x((i-1)*k+1:i*k) , length(temp));% Encoding the block
    e = 1*(rand(size(encoded)) < p);
    r_x = mod(encoded + e, 2);
    % error was introduced after encoding using BSC logic

    [d i_x]= BCHDecoder(r_x, k);
        decoded((i-1)*k+1:i*k)= i_x;
end
figure;
grid on;
semilogy(pr,a,'g--')
ylabel('BER');
xlabel('Pe');
title('BER vs Pe');
hold on;
semilogy(pr,a1,'r--')
set(gca(), 'xdir','reverse');
legend(['bch(15,7)'; 'bch(15,11)'],'location', 'northeast','orientation', 'vertical');

function [ correctedCode i_x] = BCHDecoder( r_x, k)
	% BCH Decoder
	% Works for both k=11 and k=7 
	% r_x = the received code word
	% pass k as argument
	n = 15;
	correctedCode = r_x;
	% Decoder starts here 
	g_x = [];	
	if k==11
		g_x = [1 1 0 0 1] ;% Generator function created : x4+x+1 [1 1 0 0 1]
	end

	if k==7
		g_x = [1 0 0 0 1 0 1 1 1];  % Generator function created: 1+x4+x6+x7+x8
	end
	
	% Hardcoding the galva field 
	%gField = [ 8 4 2 1 12 6 3 13 10 5 14 7 15 11 9];  % We are writing the decimal representation of corresponding alpha values
	
	gField = [ 1     0     0     0;
     0     1     0     0;
     0     0     1     0;
     0     0     0     1;
     1     1     0     0;
     0     1     1     0;
     0     0     1     1;
     1     1     0     1;
     1     0     1     0;
     0     1     0     1;
     1     1     1     0;
     0     1     1     1;
     1     1     1     1;
     1     0     1     1;
     1     0     0     1;];
	gField1 = [ 8 4 2 1 12 6 3 13 10 5 14 7 15 11 9] ;   % a0, a1, a2, a3..... a14 - Galva Field 
	%Taking decimal values of Galva Field
	%gField = de2bi(gField, 'left-msb'); % Converting the decimal values of galva field to binary

	S_1 = [ 0 0 0 0]; % Syndrome vector S(alpha)
	S_3 = [ 0 0 0 0] ;% Syndrome vector S(alpha3)

	for i=1:15
    % Calculating Syndrome values
		if(r_x(i)~=0)
		    S_1 = mod(S_1 + gField(i,:), 2); % modular 2 addition of binary values
		    S_3 = mod(S_3 + gField(mod(3*(i-1), 15)+1,:), 2); % modular 2 addition of binary values
		end
	end

	if(k==11)
		'Can only detect 1 bit error';
		if(sum(S_1)~=0)
		    'Error found at';
		    pos = find(gField1==bi2de(S_1, 'left-msb'));
		    r_x(pos) = mod(r_x(pos)+1, 2);
		    correctedCode = r_x;
		    i_x = mod(abs(deconv(r_x, g_x)), 2);
		else
		    'No error detected';
		    correctedCode = r_x;
		    i_x = mod(abs(deconv(r_x, g_x)), 2);
		end
		return; %if k==11 then program will exit here
	end

	s1 = bi2de(S_1, 'left-msb'); % Decimal value of syndrome1
	S1_cube = gField(mod(3*(find(gField1==s1)-1), 15)+1, :); % Will be used to calculate sigma2
	
	if(isempty(S1_cube))
		S1_cube = [0 0 0 0];
	end
	
	temp = mod(S1_cube + S_3, 2); % using to implement peterson's forumla -> sigma2 = (s1^3 + s3)/s1 : s1 not taken here
	if(sum(temp)~=0) % Convert this into order 1
		'not null, 2 errors exist';
		% numerator of peterson's formula was not equal to zero, that means 2
		% errors exist
		temp = find(gField1==bi2de(temp, 'left-msb'))-1-find(gField1==s1)+1; % Petersons formula done here
		if temp<0
		    temp = 15+temp; % alpha^-i = alpha^15-i
		end
		sigma2 = gField(temp+1, :); % value of sigma2 
		power_s1 = find(gField1==s1)-1; % s1 = alpha^powerofSigma1
		power_s2 = temp; % s2 = alpha^powerofSigma2
		if(isempty(power_s1))
			power_s1 =0;
		end
		if(isempty(power_s2))
			power_s2 = 0;
		end
		
		'Result';
		for i=1:15
		    % temp+1 index of sigma2
		    index = mod(power_s1+i+1, 15)+1 ;
		    index1 = mod(power_s2+2*(i+1), 15)+1; 
		    result = gField(1, :) + gField(index, :) + gField(index1, :); % 1 + sigma1*x + sigma2*x2
		    result = mod(result, 2);

		    if(sum(result)==0)
		     % 1 + sigma1*x + sigma2*x2 = 0 equation satisfied 
		        r = mod(i,15)+1; % Doing index manipulation: if B is a root then position of error = 1/B
		        pos = 16-r;
		        r_x(pos) = mod(r_x(pos)+1, 2);
		    	correctedCode = r_x;
		    end
		end
	else
		% Will come here of numerator of peterson's formula is equal to zero 
		% that means there's only 1 bit error 
		'Single bit error at';
		pos = find(gField1==s1);
		r_x(pos) = mod(r_x(pos)+1, 2);
    	correctedCode = r_x;
	end
	i_x = mod(abs(deconv(r_x, g_x)), 2);
end








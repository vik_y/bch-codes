% lab5_2 

k = 11;
blockSize = 15;
N = 100000;
N = N + k - mod(N, k); % Padding

x = randi([0 1],1, N); % Generating random code word of total size N
snr = [-5 -2 0 2 5 8 12 15]
ratio = [];
ratio0 = [];
ratio7 = [];
pulse1 = [1 1 1 1 1 1 1 1];
pulse2 = [0 0.25 0.5 0.75 1 0.75 0.5 0.25]


for j=snr
    symbols = 2*x-1;
    
    transmitted = pulseGenerator(symbols, pulse1);
    transmitted = awgn(transmitted, j);
    received = pulseDecoder(transmitted, pulse1);
    
    symbols(symbols>=0)=1;
   	symbols(symbols<0)=0;
    [num_bit,ratio_bit] = biterr(symbols, received);
    ratio0 = [ratio0 ratio_bit];
end

for j=snr
    enc = [zeros(1, floor(N/k)*15)]; % allocating memory to speed up program
    for i=1:N/k
        temp = x((i-1)*k+1:i*k);
        
        encoded = BCHEncoder( x((i-1)*k+1:i*k) , length(temp));
        enc((i-1)*15+1:i*15)= encoded;
    end
    
    enc = enc*2-1;
    transmitted = pulseGenerator(enc, pulse1);
    transmitted = awgn(transmitted, j);
    received = pulseDecoder(transmitted, pulse1);
    
    decoded = [zeros(1, N)];
    for i=1:N/k    
        [d, i_x]= BCHDecoder(received((i-1)*15+1:i*15), k);
        decoded((i-1)*k+1:i*k)= i_x;
    end
    enc; 
    x;
    [num_bit,ratio_bit] = biterr(decoded, x);
    ratio = [ratio ratio_bit];
end
% 
k = 7
for j=snr
    enc = [zeros(1, floor(N/k)*15)]; % allocating memory to speed up program
    for i=1:N/k
        temp = x((i-1)*k+1:i*k);
        
        encoded = BCHEncoder( x((i-1)*k+1:i*k) , length(temp));
        enc((i-1)*15+1:i*15)= encoded;
    end
    
    enc = enc*2-1;
    transmitted = pulseGenerator(enc, pulse1);
    transmitted = awgn(transmitted, j);
    received = pulseDecoder(transmitted, pulse1);
    
    decoded = [zeros(1, N)];
    for i=1:N/k    
        [d, i_x]= BCHDecoder(received((i-1)*15+1:i*15), k);
        decoded((i-1)*k+1:i*k)= i_x;
    end
    enc; 
    x;
    [num_bit,ratio_bit] = biterr(decoded, x);
    ratio7 = [ratio7 ratio_bit];
end
% 
% snr

figure(1);
ratio 
ratio0
ratio7
semilogy(snr, ratio0, 'r')
hold on;
semilogy(snr, ratio,'g')
hold on;
semilogy(snr, ratio7, 'b')
hold on;
ylabel('BER');
xlabel('SNR');
title('BER vs SNR');
xlim([-5, 15])
legend('No Coding', 'BCH(15, 11)', 'BCH(15, 7)')
grid on;



    